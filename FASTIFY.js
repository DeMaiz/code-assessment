class Fastify {
  constructor(logger = app.config.debug) {
    console.log('Fastify init');
    this.fastify = require('fastify')({
      logger: {
        prettyPrint: true,
        level: 'trace'
      }
    });
    return this;
  }

  async listen() {
    try {
      await this.fastify.listen(app.config.port);
      this.fastify.log.info(`server listening on ${app.config.port}`);
    } catch (err) {
      this.fastify.log.error(err.message);
      process.exit(1);
    }
  }
}

module.exports = new Fastify();
