const UserSchema = app.mongoose.Schema;
const validateEmail = function(email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const userschema = new UserSchema({
  fullName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
    validate: [validateEmail, 'Please fill a valid email address'],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please fill a valid email address'
    ]
  },
  password: {
    type: String,
    required: true
  },
  createdDate: {
    type: Number,
    default: Date.now
  },
  updatedDate: {
    type: Number,
    default: Date.now
  },
  devices: [
    {
      type: String
    }
  ],
  lastLogin: {
    type: Number,
    default: Date.now
  }
});

const _userschema = app.mongoose.model('User', userschema);
global.app.models.user = _userschema;

userschema.pre('save', next => {
  now = new Date().getTime();
  next();
});
