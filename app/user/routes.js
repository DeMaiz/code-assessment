module.exports = (fastify, iops, next) => {
  fastify.post('/register', async (req, res) => {
    const { body } = req;

    const user = app.db.create(app.models.user, body);
    res.send({
      status: true,
      message: ''
    });
  });
  fastify.post('/login', async (req, res) => {
    const { body } = req;
    const password = app.modules.bcrypt.hashSync(
      body.password,
      app.modules.bcrypt.genSaltSync(10)
    );
    console.log(password);
    try {
      const user = await app.db.select(app.models.user, {
        email: body.email,
        password
      });
      console.log(user);
    } catch (err) {
      console.log(err);
    }

    res.send({
      status: true,
      message: ''
    });
  });
  next();
};
