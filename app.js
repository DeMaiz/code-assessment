(async () => {
  const FastifyServer = require('./server');
  const server = new FastifyServer();
  await server.connectMongo();
  await server.listen();
  server.loadModels();
})();
