class MongoDB {
  constructor(options) {
    // options for the mongodb connections
    console.log('MongoDb init');
    this.mongoose = require('mongoose');
    this.options = options || app.config.mongodb.options;
    this.uri = app.config.mongodb.uri;
    app.mongoose = this.mongoose;
    return this;
  }

  connect() {
    return new Promise((res, rej) => {
      this.mongoose.connect(this.uri, this.options).then(
        () => {
          /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
          console.log('Mongodb Default connect....');
          app.mongoose = this.mongoose;
          return res(this.mongoose);
        },
        err => {
          /** handle initial connection error */
          console.log('Mongoose default connection error: ' + err);
          rej(err);
        }
      );
    });
  }

  create(model, doc) {
    return new Promise((resolve, reject) => {
      model.create(doc, (err, user) => {
        err ? reject(err) : resolve(user);
      });
    });
  }

  async select(model, query = {}) {
    return new Promise((resolve, reject) => {
      const key = app.modules.QuickEncrypt.generate(1024);
      doc.password = app.modules.QuickEncrypt.encrypt(doc.password, key.public);
      model.find(query, (err, doc) => {
        err ? reject(err) : resolve(doc);
      });
    });
  }

  selectAll() {}

  update() {}

  updateMany() {}

  updateAll() {}

  remove() {}

  removeAll() {}
}

module.exports = MongoDB;
