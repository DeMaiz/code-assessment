module.exports = class Server {
  constructor(debug) {
    global.app = {};
    this.loadModules(debug);
    this.loadMiddlewares();
  }

  loadModules(isDebug = false) {
    const requireModules = {
      QuickEncrypt: require('quick-encrypt')
    };
    global.app.config = require('./config.json');
    global.app.MongoDB = require('./common/mongodb');
    global.app.modules = requireModules;
    global.app.framework = require('./FASTIFY');
    global.app.isDebug = isDebug || global.app.config.debug;
    global.app.fastify = global.app.framework.fastify;
    global.app.models = {};
    global.app.routes = {
      user: app.fastify.register(require('./app/user/routes'), {
        prefix: '/user'
      })
    };
  }

  loadMiddlewares() {
    app.fastify.register(require('fastify-jwt'), {
      secret: app.config.secretKey
    });
    app.fastify.decorate('authenticate', async function(request, reply) {
      try {
        await request.jwtVerify();
      } catch (err) {
        reply.send(err);
      }
    });
  }

  loadModels() {
    require('./app/models/User.model');
  }

  async connectMongo() {
    app.db = new global.app.MongoDB();
    try {
      await app.db.connect();
    } catch (err) {
      console.log(err);
      process.exit(1);
    }

    return this;
  }

  async listen() {
    await global.app.framework.listen();
  }
};
